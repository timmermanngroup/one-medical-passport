<?php 
    /*
        Template name: Solutions2
    */
    get_header();
?>
<section>
    <div class="inner-banner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <h1>SOLUTIONS</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="AbourTextWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 title text-center">
                    <?php echo get_field('after_banner_text'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="Solutions SolutionsWrap main-raw bg-gry">
        <div class="container">
            <div class="row  fadeInUp wow">
                <div class="col-sm-12 col-xs-12">
                    <div class="title text-center">
                        <h2><?php echo get_field('the_result_text'); ?></h2>
                    </div>
                </div>
                <?php 
                        $args = array(
                                'post_type' => 'solutions',
                                
                            );
                        $the_query = new WP_Query( $args ); 
                        if ( $the_query->have_posts() ) : 
                            $j = 1;
                            while ( $the_query->have_posts() ) : $the_query->the_post(); 

                      ?>
                        <div class="col-sm-4 col-xs-12 solution-<?php echo $j; ?>">
                            <div class="solution-box">
                                <div class="img-wrap">
                                    <img src="<?php echo get_field('other_page_small_banner',get_the_ID()); ?>" alt="online pre admission"
                                        class="img-responsive" />
                                </div>
                                <div class="text-wrap">
                                    <i><img src="images/ic-pre-admission.svg" alt="online pre admission"
                                            class="img-responsive"></i>
                                    <h4><?php echo get_the_title(); ?> </h4>
                                    <p>Gather patient-entered histories, medications, and demographics </p>
                                    <a href="online-pre-admission.html">Learn more
                                        <span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                width="18.345px" height="10.786px" viewBox="0 0 18.345 10.786"
                                                enable-background="new 0 0 18.345 10.786" xml:space="preserve">
                                                <line fill-rule="evenodd" clip-rule="evenodd" fill="none"
                                                    stroke-width="1.97" stroke-linecap="square" stroke-linejoin="round"
                                                    stroke-miterlimit="10" x1="0.985" y1="5.393" x2="15.543" y2="5.393" />
                                                <polyline fill-rule="evenodd" clip-rule="evenodd" fill="none"
                                                    stroke-width="1.97" stroke-linecap="square" stroke-linejoin="round"
                                                    stroke-miterlimit="10" points="
                                            13.478,1.393 17.36,5.393 13.478,9.393 " /></svg>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                <?php 
                    $j++;
                    endwhile;
                    wp_reset_query();
                    endif;
                ?>
            </div>
        </div>
    </div>

    <!-- <div class="AlreadyWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-right">
                    <div class="BoxWrap text-left">
                        <h6>READY TO LEARN WHAT</h6>
                        <h2>750+ ASCs Already Know<span class="Question">?</span></h2>
                        <a href="#" class="btn-primary">SCHEDULE A DEMO TODAY!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="SubscribeWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center">
                    <h2>Want to learn how One Medical Passport<br> has helped ASCs to deliver great care<span
                            class="Question">?</span> </h2>
                    <div class="text-wrap">
                        <h6>SUBSCRIBE TO OUR NEWSLETTER NOW</h6>
                        <div class="download-form">
                            <form>
                                <input type="text" class="form-control" placeholder="Enter your email" required="">
                                <button type="submit" class="btn-primary">SUBSCRIBE NOW</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="AlreadyWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-right">
                    <?php dynamic_sidebar('first-cta'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="SubscribeWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center">
                    <?php //dynamic_sidebar('second-cta'); ?>
					<h2><?php echo get_field('cta_tagline'); ?><span class="Question">?</span></h2>
					<div class="text-wrap">
					<h6>SUBSCRIBE TO OUR NEWSLETTER NOW</h6>
					<div class="download-form"><?php echo do_shortcode("[email-subscribers-form id='1']"); ?></div>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>