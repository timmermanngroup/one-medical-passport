<?php 
    /*
        Template name: Patient Tracking
    */
    get_header();
?>
<section>
    <div class="inner-banner SolutionsBanner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <?php echo get_field('banner_text'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="patient-track-solutions-raw float">
        <div class="container">
            <div class="patient-track-solutions-col float">
                <div class="row">
                    <div class="col-sm-6 col-xs-12 left-conn">
                        <div class="title">
                            <?php echo get_field('patient_tracking_solution_content'); ?>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 right-conn">
                        <img class="img-responsive" src="<?php echo get_field('patient_tracking_solution_image'); ?>" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="patient-track-questions-raw float">
        <div class="container">
            <div class="row">
                <?php
                    if( have_rows('patient_tracking_solutions') ):
                    while ( have_rows('patient_tracking_solutions') ) : the_row();
                ?>
                <div class="col-sm-12 col-xs-12 patient-track-questions-col title">
                    <?php echo get_sub_field('patient_tracking_question_answer'); ?>
                </div>
                <?php 
                    endwhile;
                    endif;    
                ?>
            </div>
        </div>
    </div>
    <div class="footer-raw float">
        <div class="container">
            <p>@2013-<?php echo date('Y'); ?> One Medical Passport, Inc. All rights reserved.</p>
            <div class="social">
                <ul>
                    <?php
    
                if( have_rows('social_media','options') ):
                    while ( have_rows('social_media','options') ) : the_row();
                    ?>
                    <li>
                        <a href="<?php echo get_sub_field('social_media_url','options'); ?>" target="_blank">
                            <?php echo get_sub_field('social_media_icon','options'); ?>
                        </a>
                    </li>
                    <?php 
                    endwhile;
                endif;    
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>