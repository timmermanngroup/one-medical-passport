<?php 
    /*
        Template name: Services
    */
    get_header();
?>
<section>
    <div class="inner-banner" style="background: url('<?php echo get_field('banner_image'); ?>');">
        <div class="container">
            <div class="inner_con">
                <div class="Box fadeInUp wow">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="ServicesWrapper ServicesBoxWrap text-center float">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 TextWrap fadeInUp wow">
                    <h2><?php echo get_field('after_banner_tagline'); ?></h2>
                </div>
            </div>
            <div class="row">
                <?php

                // check if the repeater field has rows of data
                if( have_rows('services') ):
                
                 	// loop through the rows of data
                    while ( have_rows('services') ) : the_row();
                    ?>
                <div class="col-sm-4 ServiceBox fadeInUp wow" data-wow-delay="0.1s">
                    <div class="ImgBox">
                        <img src="<?php echo get_sub_field('service_icon'); ?>" alt="<?php echo get_sub_field('service_title'); ?>" class="img-responsive">
                        <h3><span><?php echo get_sub_field('service_title'); ?></span></h3>
                    </div>
                    <div class="TextBox">
                        <p><?php echo get_sub_field('service_text'); ?></p>
                    </div>
                </div>
               <?php 
                endwhile;
                endif;
               ?>

            </div>
        </div>
    </div>
<?php 
    get_footer();
?>