<?php 
    /*
        Template name: Blog
    */
    get_header();
?>
    <section>
        <div class="inner-banner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
            <div class="inner-main">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                            <h1>BLOG</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blogwrap section">
           <div class="section main-raw recent-article"> 
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                     <?php 
                    $args = array(
                            'post_type'   => 'post',
                            'posts_per_page' => 1
                           
                    );
                    $query = new WP_Query( $args ); ?>
					<?php if ( $query->have_posts() ) : ?>
						<?php while ( $query->have_posts() ) : $query->the_post(); 
                        ?>
							<div class="blog-box"> 
                                <a href="<?php echo get_the_permalink(); ?>" class="img-wrap">
                                    <?php $img_url =  wp_get_attachment_url( get_post_thumbnail_id()); ?>
                                    <img src="<?php echo $img_url ?>" class="img-responsive" alt="<?php echo get_the_title(); ?>"> 
                                </a>
                                <div class="text-wrap">
                                    <div class="date-box"><?php echo get_the_date('d'); ?><span><?php echo get_the_date('M'); ?></span></div>
                                    <a href="<?php echo get_the_permalink(); ?>" class="blog-title"><?php echo get_the_title(); ?>...</a>
                                       
                                     <p><?php echo get_the_excerpt(); ?></p>
                                        
                                </div>
                           </div>  
							<?php 
							$exclude_post_id = get_the_ID();
								endwhile;
							wp_reset_query();
							endif;
							?>
                        </div> 
                        <div class="col-sm-4 col-xs-12">
                            <div class="recent-list">
                                <ul>
                                <?php 
                    $args = array(
                            'post_type'   => 'post',
                            'posts_per_page' => 3,
							'post__not_in' => array($exclude_post_id)
                           
                    );
                    $query = new WP_Query( $args ); ?>
					<?php if ( $query->have_posts() ) : ?>
						<?php while ( $query->have_posts() ) : $query->the_post(); 
                        ?>
								<li>
                                    <a href="<?php echo get_permalink(); ?>">
                                        <div class="article-thumb">
                                            <?php $img_url =  wp_get_attachment_url( get_post_thumbnail_id()); ?>
                                    		<img src="<?php echo $img_url ?>" class="img-responsive" alt="<?php echo get_the_title(); ?>"> 
                                        </div>
                                        <span><?php echo get_the_date('d'); ?><?php echo get_the_date('M'); ?></span>
                                        <h6><?php echo get_the_title(); ?></h6>
                                    </a>
                                </li>
                               <?php 
									endwhile;
									wp_reset_query();
									endif;
									?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
			<div class="section bg-gry main-raw"> 
    			<div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 title">
                            <h2>Past Blogs</h2>
                        </div>
                    </div>
                    <div class="row">
                        <?php 

                        // the query
                        $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
                        $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => 20,
                                'paged' => $paged,
    						'offset' => 4
                                
                                
                            );
                         $the_query = new WP_Query( $args ); 
                        if ( $the_query->have_posts() ) :  ?>

                            <?php  while ( $the_query->have_posts() ) : $the_query->the_post();
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="blog-box">
                                    <a href="<?php echo get_the_permalink(); ?>" class="img-wrap">
                                        <?php $img_url =  wp_get_attachment_url( get_post_thumbnail_id()); ?>
                                        <img src="<?php echo $img_url ?>" class="img-responsive" alt="<?php echo get_the_title(); ?>"> 
                                    </a>
                                    <div class="text-wrap">
                                        <div class="date-box"><?php echo get_the_date('d'); ?><span><?php echo get_the_date('M'); ?></span></div>
                                        <a href="<?php echo get_the_permalink(); ?>" class="blog-title"><?php echo get_the_title(); ?></a>
                                        <p><?php echo get_the_excerpt(); ?></p>
                                        <a href="<?php echo get_the_permalink(); ?>" class="more-btn"></a>
                                    </div>
                                </div>
                            </div>
                        <?php 
                            endwhile;
                            endif;
                        ?>
                        <div class="col-sm-12 col-xs-12 text-center ">
                           <?php
                           $big = 999999999; // need an unlikely integer

                                echo paginate_links( array(
                                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                        'format' => '?paged=%#%',
                                        'current' => max( 1, get_query_var('page') ),
                                        'total' => $the_query->max_num_pages,
                                        'prev_text'          => __('«'),
                                        'next_text'          => __('»')
                                ) );
                            ?>
                        </div>
                    </div>
                </div>
			</div>
        </div>




        <div class="AlreadyWrapper main-raw">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-right">
                        <?php dynamic_sidebar('first-cta'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="SubscribeWrap main-raw">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-center">
                        <?php //dynamic_sidebar('second-cta'); ?>
						<h2><?php echo get_field('cta_tagline'); ?><span class="Question">?</span></h2>
					<div class="text-wrap">
					<h6>SUBSCRIBE TO OUR NEWSLETTER NOW</h6>
					<div class="download-form"><?php echo do_shortcode("[email-subscribers-form id='1']"); ?></div>
					</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 
    get_footer();
?>