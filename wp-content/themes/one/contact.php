<?php 
    /*
        Template name: Contact
    */
    get_header();
?>
<section>
    <div class="inner-banner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <h1>contact us</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=" main-raw">
        <div class="container">
            <div class="contact-main row">
                <div class="col-sm-7 col-xs-12">
                    <div class="location-map">
                        <div id="map"></div>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12">
                    <div class="contact-list">
                        <ul>
                            <?php if(get_field('address')){ ?>
                            <li>
                                <i>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic-location.svg" class="img-responsive" alt="location">
                                </i>
                                <h4><?php echo get_field('address_title'); ?></h4>
                                <p><?php echo get_field('address'); ?></p>
                            </li>
                            <?php } if(get_field('sales_phone')){ ?>
                            <li>
                                <i>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic-phone.svg" class="img-responsive" alt="phone">
                                </i>
                                <h4><?php echo get_field('sales_title'); ?></h4>
                                <a href="tel:<?php echo get_field('sales_phone_dial'); ?>"><?php echo get_field('sales_phone'); ?></a>
                            </li>
                            <?php } if(get_field('patient_phone')){ ?>
                            <li>
                                <i>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic-phone.svg" class="img-responsive" alt="phone">
                                </i>
                                <h4><?php echo get_field('patient_phone_title'); ?></h4>
                                <a href="tel:<?php echo get_field('patient_phone_dial'); ?>"><?php echo get_field('patient_phone'); ?></a>
                            </li>
                            <?php } if(get_field('fax')){ ?>
                            <li>
                                <i>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic-fax.svg" class="img-responsive" alt="fax">
                                </i>
                                <h4><?php echo get_field('fax_title'); ?></h4>
                                <P><?php echo get_field('fax'); ?></P>
                            </li>
                            <?php } if(get_field('email')){ ?>
                            <li>
                                <i>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic-email.svg" class="img-responsive" alt="email">
                                </i>
                                <h4><?php echo get_field('email_title'); ?></h4>
                                <a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-raw bg-gry">
        <div class="container">
            <div class="contact-main row">
                <div class="col-sm-6 col-xs-12">
                    <div class="support-box">
                        <h4><?php echo get_field('facility_support_title'); ?></h4>
                        <p><?php echo get_field('facility_support_text'); ?></p>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="support-box">
                        <h4><?php echo get_field('patient_support_title'); ?></h4>
                        <p><?php echo get_field('patient_support_text'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>