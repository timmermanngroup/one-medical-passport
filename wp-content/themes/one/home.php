<?php 
    /*
        Template name: Home
    */
    get_header();
?>
<section>
    <div id="myCarousel" class="hero section carousel slide carousel-fade" data-ride="carousel" data-interval="10000" data-pause="false">
        <div class="carousel-inner">
            <?php
                if( have_rows('banners') ):
                $i = 1;
                while ( have_rows('banners') ) : the_row();
            ?>
                <div class="item <?php if($i == 1){ ?>active<?php } ?>" style="background: url(<?php echo get_sub_field('banner_image'); ?>)">
                    <div class="hero-main">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-10 col-xs-12 hero-con fadeInUp wow">
                                    <h2><?php echo get_sub_field('banner_text'); ?></h2>
                                    <span><?php echo get_sub_field('banner_sub_text'); ?></span>
                                    <button class="btn-primary" data-toggle="modal" data-target="#schedule-modal"><?php echo get_sub_field('banner_cta_label'); ?></button>
                                </div>
                                <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next"></a> -->
                            </div>
                        </div>
                    </div>
                </div>
            <?php 
                $i++;
                endwhile;
                endif;
            ?>
        </div>
        
        <!-- <div class="scroll-btm">
            <div class="scroll-down"></div>
        </div> -->
    </div>
    <div class="delivering-raw main-raw bg-gry">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-xs-12 fadeInUp wow">
                    <div class="text-wrap">
                        <div class="title">
                            <h1 id="home-h1"><?php echo get_field('after_banner_section_title'); ?></h1>
                        </div>
                        <?php echo get_field('after_banner_section_text'); ?>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12 fadeInRight wow">
                    <div class="img-wrap">
                        <img src="<?php echo get_field('after_banner_secttion_image'); ?>" class="img-responsive"
                            alt="one medica one-mediacl-dashbord">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="howto-raw main-raw">
        <div class="container">
            <div class="title text-center fadeInUp wow">
                <h2><?php echo get_field('how_to_get_title'); ?></h2>
            </div>
            <div class="row text-center  fadeInUp wow">
                <?php

                // check if the repeater field has rows of data
                if( have_rows('how_to_get_process') ):
                    $i = 1;
                    while ( have_rows('how_to_get_process') ) : the_row();
                    ?>
                <div class="col-sm-4 col-xs-12 started-step">
                    <div class="img-wrap">
                        <i>
                            <a href="/easily-schedule-a-demo/"><img src="<?php echo get_sub_field('box_image'); ?>" class="img-responsive" alt="schedule" /></a>
                            <span class="step-no"><?php echo $i; ?></span>
                        </i>
                    </div>
                    <p><?php echo get_sub_field('box_title'); ?></p>
                </div>
                <?php 
                    $i++;
                    endwhile;
                endif;
                ?>
                <div class="col-sm-12 col-xs-12 asc-trust fadeInUp wow">
                    <div class="BorderBoxWrap">
                        <h3><?php echo get_field('find_out_tagline'); ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Solutions main-raw bg-gry">
        <div class="container">
            <div class="row  fadeInUp wow">
                <div class="col-sm-12 col-xs-12">
                    <div class="title text-center">
                        <h2><?php echo get_field('solutions_title'); ?></h2>
                    </div>
                </div>
                <?php 
                        $args = array(
                                'post_type' => 'solutions',
                                
                            );
                        $the_query = new WP_Query( $args ); 
                        if ( $the_query->have_posts() ) : 
                            $j = 1;
                            while ( $the_query->have_posts() ) : $the_query->the_post(); 

                      ?>
                        <div class="col-sm-4 col-xs-12 solution-<?php echo $j; ?>">
                            <div class="solution-box">
                                <div class="img-wrap">
                                    <img src="<?php echo get_field('other_page_small_banner',get_the_ID()); ?>" alt="online pre admission"
                                        class="img-responsive" />
                                </div>
                                <div class="text-wrap">
                                    <i><img src="<?php echo get_field('other_page_icon',get_the_ID()); ?>" alt="online pre admission"
                                            class="img-responsive"></i>
                                    <h4><?php echo get_the_title(); ?> </h4>
                                    <p><?php echo get_field('for_other_page_tagline',get_the_ID()); ?> </p>
                                    <a href="<?php echo get_the_permalink(get_the_ID()); ?>">Learn more
                                        <span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                width="18.345px" height="10.786px" viewBox="0 0 18.345 10.786"
                                                enable-background="new 0 0 18.345 10.786" xml:space="preserve">
                                                <line fill-rule="evenodd" clip-rule="evenodd" fill="none"
                                                    stroke-width="1.97" stroke-linecap="square" stroke-linejoin="round"
                                                    stroke-miterlimit="10" x1="0.985" y1="5.393" x2="15.543" y2="5.393" />
                                                <polyline fill-rule="evenodd" clip-rule="evenodd" fill="none"
                                                    stroke-width="1.97" stroke-linecap="square" stroke-linejoin="round"
                                                    stroke-miterlimit="10" points="
                                            13.478,1.393 17.36,5.393 13.478,9.393 " /></svg>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                <?php 
                    $j++;
                    endwhile;
                    wp_reset_query();
                    endif;
                ?>
            </div>
        </div>
    </div>
    <div class="ourword-raw main-raw bg-blue">
        <div class="container">
            <div class="row text-center fadeInUp wow">
                <div class="col-sm-12 col-xs-12">
                    <div class="title text-center">
                        <h2><?php echo get_field('take_our_word_title'); ?></h2>
                        <p><?php echo get_field('take_our_word_subtitle'); ?></p>
                    </div>
                </div>
                <?php

            if( have_rows('numbers') ):
                while ( have_rows('numbers') ) : the_row();
                ?>
                <div class="col-sm-3 col-xs-6 percent-value">
                    <i><img src="<?php echo get_sub_field('icon'); ?>" alt="<?php echo get_sub_field('title'); ?>" class="img-responsive"></i>
                    <span class="counter" data-count="<?php echo get_sub_field('number'); ?>"></span>
                    <h6><?php echo get_sub_field('title'); ?></h6>
                </div>
                <?php 
                endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
    <div class="work-raw main-raw">
        <div class="container">
            <div class="row fadeInUp wow">
                <div class="col-sm-12 col-xs-12">
                    <div class="title text-center">
                        <h2><?php echo get_field('testimonial_label'); ?></h2>
                    </div>
                </div>
                <?php

                // check if the repeater field has rows of data
                if( have_rows('testimonials') ):
                
                    // loop through the rows of data
                    while ( have_rows('testimonials') ) : the_row();
                    ?>
                <div class="col-sm-6 col-xs-12">
                    <a href="#" class="work-box">
                        <div class="text-wrap">
                            <p> <?php echo get_sub_field('text'); ?>
                            </p>
                            <div class="work-by">
                                <h5><?php echo get_sub_field('author'); ?></h5>
                                <span> <?php echo get_sub_field('author_designation'); ?></span>
                                <i><img src="<?php echo get_sub_field('author_image'); ?>" alt="#" class="img-responsive"></i>
                            </div>
                        </div>
                        
                    </a>
                </div>
                <?php 
                    endwhile;
                    endif;
                ?>
            </div>
        </div>
    </div>
    <style>
    .cta-raw {

        background: #EEEBE8 url(<?php echo get_field('ready_to_run_image'); ?>) no-repeat left center;
        position: relative;
    
    }
    </style>
    <div class="cta-raw main-raw">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-6 col-xs-12 pull-right fadeInRight wow">
                    <h2><?php echo get_field('ready_to_run_tagline'); ?></h2>
                    <div class="text-wrap">
                        <h6><?php echo get_field('download_text'); ?></h6>
                        <h4><?php echo get_field('6_steps_text'); ?> </h4>
                        <div class="download-form">
                            <form>
                                <input type="text" class="form-control" placeholder="Enter your email" required>
                                <button type="submit" class="btn-primary">DOWNLOAD NOW</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="schedule-raw main-raw bg-blue">
        <div class="container">
            <div class="row schedule-main">
                <div class="col-sm-7 col-xs-12 fadeInLeft wow">
                    <div class="text-wrap">
                        <?php echo get_field('cta_tagline'); ?>
                        
                        <button class="btn-primary" data-toggle="modal" data-target="#schedule-modal"><?php echo get_field('cta_label'); ?></button>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12 fadeInRight wow">
                    <div class="img-wrap">
                        <img src="<?php echo get_field('cta_image'); ?>" class="img-responsive" alt="schedule demo">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>