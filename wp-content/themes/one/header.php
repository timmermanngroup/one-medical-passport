<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="theme-color" content="#2B2D78" />
    <meta name="viewport" content="width=device-width" />
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
        integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest" crossorigin="use-credentials">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#2b2f78">
	<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<header id="header" class="float-panel" data-top="0" data-scroll="200">
    <div class="header">
        <div class="header-main">
            <div class="logo">
                <a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php echo get_field('logo','options'); ?>" class="img-responsive"
                        alt="one-medical" /></a>
            </div>
            <div class="header-right">
                <div class="head-action">
                    <ul>
                        <li><a href="<?php echo get_field('patient_url','options'); ?>" target="_blank">Patients</a></li>
                        <li><a href="<?php echo get_field('staff_login_url','options'); ?>" target="_blank">Staff Sign In</a></li>
                    </ul>
                </div>
                <div class="nav">
                    <div class="nav-main">
                        <?php wp_nav_menu(array('menu' => 'Main Menu')); ?>
                    </div>
                </div>
                <div class="res_navigation">
                    <a href="javascript:void(0)" class="nav-toggle" id="trigger-overlay">
                        <span class="top"></span>
                        <span class="middle"></span>
                        <span class="bottom"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
