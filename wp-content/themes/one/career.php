<?php 
    /*
        Template name: Career
    */
    get_header();
?>
<section>
    <div class="inner-banner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <h1>careers</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="AbourTextWrap  main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 title">
                     <?php echo get_field('after_banner_text'); ?>
                </div>
            </div>
            <div class="positions section">
                <div class="title text-center">
                    <h2><?php echo get_field('open_position_title'); ?></h2>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-center">
                        <div class="positions-box">
                            <div class="FaqBox">
                                <div class="panel-group" id="accordion<?php echo $i; ?>">
                                    <?php
                                    if( have_rows('faqs') ):
                                    $j = 1;
                                    while ( have_rows('faqs') ) : the_row();
                                ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion<?php echo $i; ?>" href="#collapse<?php echo $i.$j; ?>"><span></span><h4><?php echo get_sub_field('question'); ?></h4></a>
                                            </div>
                                        </div>
                                        <div id="collapse<?php echo $i.$j; ?>" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <?php echo get_sub_field('answer'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                    $j++;
                                        endwhile;
                                    endif;
                                    ?>
                                </div>
                            </div>
                            <!-- <ul>
                                <?php

                                if( have_rows('open_positions') ):
                                    while ( have_rows('open_positions') ) : the_row();
                                    ?>
                                <li>
                                    <h4><?php echo get_sub_field('position_title'); ?></h4>
                                    <?php echo get_sub_field('position_description'); ?>
                                    <a href="#"></a>
                                </li>
                                <?php 
                                    endwhile;
                                endif;    
                                ?>
                            </ul> -->
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>