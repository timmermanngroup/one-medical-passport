<?php 
    /*
        Template name: Difference
    */
    get_header();
?>
<section>
    <div class="inner-banner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <h1>OUR DIFFERENCE</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="AbourTextWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
   <div class="AlreadyWrapper main-raw" style="background-image: url(<?php echo get_field('cta_image'); ?>)">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-right">
                    <div class="textwidget custom-html-widget"><div class="BoxWrap text-left">
						<h6>WANT TO SEE</h6>
						<h3>the 1MP difference in action?</h3> 
						<a class="btn-primary" data-toggle="modal" data-target="#schedule-modal" rel="noopener noreferrer">SCHEDULE A DEMO TODAY!</a></div></div>                </div>
            </div>
        </div>
    </div>
    
</section>

<?php 
    get_footer();
?>