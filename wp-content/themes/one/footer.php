<footer>
    <div class="footer section">
        <div class="footer-top section">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                        <div class="footer-col footer-nav hidden-xs">
                            <h6>COMPANY</h6>
                            <?php echo wp_nav_menu(array('menu' => 'Footer Menu')); ?>
                        </div>
                        <div class="footer-col contact-info">
                            <h6><?php echo get_field('contact_sales_label','options'); ?></h6>
                            <ul>
                                <li>
                                    <i><img src="<?php echo get_template_directory_uri(); ?>/images/ic-email.svg" alt="email" class="img-responsive"></i>
                                    <h5><?php echo get_field('email_label','options'); ?></h5>
                                    <a href="mailto:<?php echo get_field('email','options'); ?>"><?php echo get_field('email','options'); ?></a>
                                </li>
                                <li>
                                    <i><img src="<?php echo get_template_directory_uri(); ?>/images/ic-phone.svg" alt="phone" class="img-responsive"></i>
                                    <h5><?php echo get_field('phone_label','options'); ?></h5>
                                    <a href="tel:8005407527" class="green"><?php echo get_field('phone','options'); ?></a>
                                </li>
								<li>
                                    <i class="fas fa-user-friends" style="color: #2b3176; padding-right: 10px;"></i>
                                    <h5>REFER A FRIEND</h5>
                                    <a href="https://info.1mp.com/refer-a-friend">Click here to learn about our referral program</a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-col contact-support">
                            <h6><?php echo get_field('contact_support_label','options'); ?></h6>
                            <ul>
                                <li>
                                    <h5><?php echo get_field('facilities_label','options'); ?></h5>
                                    <?php echo get_field('facilities_text','options'); ?>
                                    
                                </li>
                                <li>
                                    <h5><?php echo get_field('patient_label','options'); ?></h5>
                                    <p><?php echo get_field('patient_text','options'); ?>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 text-center">
                        <div class="logo">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/one-medical-logo-dark.svg" alt="one medical" class="img-responsive">
                        </div>
                        <div class="social">
                            <ul>
                                <?php

                            if( have_rows('social_media','options') ):
                                while ( have_rows('social_media','options') ) : the_row();
                                ?>
                                <li>
                                    <a href="<?php echo get_sub_field('social_media_url','options'); ?>" target="_blank">
                                        <?php echo get_sub_field('social_media_icon','options'); ?>
                                    </a>
                                </li>
                                <?php 
                                endwhile;
                            endif;    
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-btm section">
            <div class="container">
               <?php echo get_field('copyright_text','options'); ?>
                <p class="privacy"><a href="/privacy-policy/">Privacy Policy & Terms of Service</a></p>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div id="schedule-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <div class="title text-center">
                        <h2>Ready to take a closer look<span class="Question">?</span></h2>
                        <p>Request a quick demo and find out how One Medical Passport can positively impact your
                            perioperative process and help you deliver a better patient care experience. </p>
                    </div>
                    <div class="schedule-form">
                        <?php ## echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
							<!--[if lte IE 8]>
							<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
							<![endif]-->
							<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
							<script>
							  hbspt.forms.create({
								portalId: "5271895",
								formId: "2ae870d7-c3c4-4aa3-b652-15547dca7ad8",
								sfdcCampaignId: "7014A0000013fuAQAQ",
								css: "false"
							});
							</script>

                    </div>
                </div>

            </div>

        </div>
    </div>
</footer>

<div class="video-modal">
    <div id="video-modal-content" class="video-modal-content">
        <iframe id="youtube" allow="autoplay" allowfullscreen></iframe>
        <a href="#" class="close-video-modal"></a>
    </div>
    <div class="overlay"></div>
</div>
<?php wp_footer(); ?>
   
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5271895.js"></script>
<!-- End of HubSpot Embed Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141158455-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-141158455-1', { 'optimize_id': 'GTM-KG7H7X7'});
</script>


</body>
</html>