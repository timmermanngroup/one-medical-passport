if (document.documentElement.clientWidth < 768) {

    (function($){
    $(document).ready(function(){
    $('.nav-main ul li').children('ul').toggle();
        $('.nav-main ul li.menu-item-has-children span').on('click', function(){
            $(this).removeAttr('href');
            var element = $(this).parent('li');
            if (element.hasClass('open')) {
                element.removeClass('open');
                element.find('li').removeClass('open');
                element.find('ul').slideUp(200);
            }
            else {
                element.addClass('open');
                element.children('ul').slideDown(200);
                element.siblings('li').children('ul').slideUp(200);
                element.siblings('li').removeClass('open');
                element.siblings('li').find('li').removeClass('open');
                element.siblings('li').find('ul').slideUp(200);
            }
        });
    
    
    });
    })(jQuery);
    
    
    }

$(document).ready(function(){
    function toggle_video_modal() {
        $(".js-trigger-video-modal").on("click", function(e){
            e.preventDefault();
          var id = $(this).attr('data-youtube-id');
          var autoplay = '?autoplay=1';
          var related_no = '?rel=0';
          var src = 'https://www.youtube.com/embed/'+id+autoplay+related_no;
          $("#youtube").attr('src', src);
          $("body").addClass("show-video-modal noscroll");
      });

      function close_video_modal() {
        event.preventDefault();
        $("body").removeClass("show-video-modal noscroll");
        $("#youtube").attr('src', '');  
      }
        $('body').on('click', '.close-video-modal, .video-modal .overlay', function(event) {
          close_video_modal();
      });
      $('body').keyup(function(e) {
          if (e.keyCode == 27) { 
            close_video_modal();
          }
      });
    }
    toggle_video_modal();
});

$('body').scrollspy({target: "", offset:60}); 
    $('a[href^="#"]').on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;
    /*
    $('html, body').stop().animate({
        scrollTop: $( $(this).attr('href') ).offset().top - 90
    }, 600);
    */
    window.location.hash = hash;
    
    
    }  // End if
})

$( ".page-id-307" ).addClass( "common" );

new WOW().init();
$('<span></span>').insertAfter('#menu-main-menu li.menu-item-has-children > a');
$('#menu-main-menu li.menu-item-type-custom:last-child a').attr('data-toggle','modal');
$('#menu-main-menu li.menu-item-type-custom:last-child a').attr('data-target','#schedule-modal');
$(document).ready(function() {
$(".nav-toggle").click(function() {
	$(this).toggleClass("active");
	$(".nav-main").toggleClass("open");
    $("body").toggleClass("nav-open");
    $(html).toggleClass("nav-open");
	
});

if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
    $(".intervent_list ul li").click(function(){
        //we just need to attach a click event listener to provoke iPhone/iPod/iPad's hover event
        //strange
    });
	$(".partenaires_con ul li").click(function(){
        //we just need to attach a click event listener to provoke iPhone/iPod/iPad's hover event
        //strange
    });
}
});

$('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');

    $({
        countNum: $this.text()
    }).animate({
        countNum: countTo
        },

        {
        duration: 2000,
        easing: 'linear',
        step: function() {
            $this.text(commaSeparateNumber(Math.floor(this.countNum)));
        },
        complete: function() {
            $this.text(commaSeparateNumber(this.countNum));
            //alert('finished');
        }
        }
    );

    });

    function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

/*
$(".carousel").swipe({
    var swipe = function (event, direction, distance, duration, fingerCount, fingerData) {
        if (direction == 'left') $(this).carousel('next');
        if (direction == 'right') $(this).carousel('prev');
    },
    allowPageScroll: "vertical"
});
*/

$(".scroll-down").click(function (){
	$('html, body').animate({
		scrollTop: $(".main-raw").offset().top - 75
	},800);
});

$('.carousel').carousel({
  interval: 10000,
  pause: false
})


