<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 <section>
        <div class="commonwrap section">
            <div class="container">
                <h1><?php get_the_title(); ?></h1>
            </div>
        </div>
	 <?php the_content(); ?>
</section>	 
<?php endwhile; endif; ?>


<?php get_footer(); ?>