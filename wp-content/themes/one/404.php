<?php 
    /*
        Template name: 404
    */
    get_header();
?>
<section>
    <div class="commonwrap section">
        <div class="container">
            <h1>Page not found</h1>
        </div>
    </div>
    <div class="ErrorWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="Img">
                        <img src="https://onemedicalpass.wpengine.com/wp-content/uploads/2019/05/ic-404.svg">
                    </div>
                    <p>The page you are looking for doesn't <br>exist or has been moved.</p>
                    <a href="https://onemedicalpass.wpengine.com" class="btn-primary">Take me home</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>