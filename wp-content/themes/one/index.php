<?php get_header(); ?>
<section>
    <div class="PostWrpa">
        <div class="container">
            <div class="ArticleMain">
                <div class="ArticleList">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="ArticaleBox">
                        <a href="<?php echo get_permalink(); ?>" class="Imgwrap">
                            <?php $img_url =  wp_get_attachment_url( get_post_thumbnail_id()); ?>
                            <img src="<?php echo $img_url; ?>" alt="<?php echo get_the_title(); ?>" class="img-responsive"> 
                            <div class="ArticalInfo">
                                <span class="date"><?php echo get_the_date('d M Y'); ?> </span>
                                <span class="autho"><?php echo get_the_author(); ?></span>       
                            </div>
                        </a>
                        <div class="Textwrap">
                            <h3><?php echo get_the_title(); ?></h3>
                            <p><?php echo get_the_excerpt(); ?></p>
                            <a href="<?php echo get_permalink(); ?>" class="more">Read more</a>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                </div>
                <div class="BLogSidebar">
                    <div class="RecentPost">
                        <h3>Recent post</h3>
                        <ul>
                            <?php 
								// the query
								

								$args = array(
									'post_type' => 'post',
									'posts_per_page' => 3,
									'order'=> 'DESC'
								);
								$the_query = new WP_Query( $args ); ?>

								<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <li>
                                    <a href="#">
                                        <div class="Imgwrap">
                                            <?php $img_url =  wp_get_attachment_url( get_post_thumbnail_id()); ?>
                                            <img src="<?php echo $img_url; ?>" alt="<?php echo get_the_title(); ?>" class="img-responsive"> 
                                        </div>
                                        <h5><?php echo get_the_title(); ?></h5>
                                    </a>
                                </li>
                            <?php
                            endwhile;
                            endif;
                            ?>
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>