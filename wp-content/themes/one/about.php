<?php 
    /*
        Template name: About
    */
    get_header();
?>
<section>
    <div class="inner-banner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <h1>Ambulatory Surgery Center Solutions</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="AbourTextWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <?php echo get_field('after_banner_text'); ?>
                </div>
                <div class="col-sm-12 col-xs-12 text-center">        
                    <br>
                    <div style="max-width: 800px; margin: 1.2em auto;">
						
<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://player.vimeo.com/video/356941200' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
<script src="https://player.vimeo.com/api/player.js"></script>
                    </div>
                    <a class="btn-primary" data-toggle="modal" data-target="#schedule-modal" rel="noopener noreferrer"><?php echo get_field('interested_in_joining_label'); ?></a>
                 </div>
            </div>
        </div>
    </div>
    <div class="TeamWrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 TextWrap text-center">
                    <?php echo get_field('only_as_good_text'); ?>
                </div>
                <div class="col-sm-12 col-xs-12 OurTeamWrap">
                    <div class="title text-center">
                        <h2><?php echo get_field('our_team_title'); ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="TeamList section">
            <ul>
                <?php
                    if( have_rows('one_medical_team') ):
                        while ( have_rows('one_medical_team') ) : the_row();
                        ?>
                        <li>
                            <div class="TeamBox">
                                <div class="img-wrap">
                                    <img src="<?php echo get_sub_field('member_avtar'); ?>" class="img-reaponsive" alt="<?php echo get_sub_field('member_name'); ?>">
                                </div>
                                <div class="text-wrap">
                                    <h3><?php echo get_sub_field('member_name'); ?></h3>
                                    <h6><?php echo get_sub_field('position'); ?></h6>
                                </div>
                            </div>
                        </li>
                <?php 
                    endwhile;
                    endif;    
                ?> 
            </ul>
        </div>
    </div>

    <div class="AdvisorsWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 TeamBoxWrap">
                    <div class="title text-center">
                        <h2><?php echo get_field('advisors_title'); ?></h2>
                    </div>
                    <ul>
                        <?php
                    if( have_rows('advisors') ):
                        while ( have_rows('advisors') ) : the_row();
                        ?>
                        <li>
                            <div class="Box">
                                <div class="TextBox">
                                    <h5><?php echo get_sub_field('name'); ?></h5>
                                    <?php if(get_sub_field('designation')){ ?>
                                        <h6><?php echo get_sub_field('designation'); ?></h6>
                                    <?php } ?>
                                </div>
                            </div>
                        </li>
                        <?php 
                        endwhile;
                    endif;    
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="AlreadyWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-right">
                    <?php dynamic_sidebar('first-cta'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="SubscribeWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center">
                    <?php //dynamic_sidebar('second-cta'); ?>
					<h2><?php echo get_field('cta_tagline'); ?><span class="Question">?</span></h2>
					<div class="text-wrap">
					<h6>SUBSCRIBE TO OUR NEWSLETTER NOW</h6>
					<div class="download-form"><?php echo do_shortcode("[email-subscribers-form id='1']"); ?></div>
					</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>