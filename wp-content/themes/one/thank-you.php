<?php 
    /*
        Template name: Thank You
    */
    get_header();
?>
<section>
    <div class="inner-banner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <!-- <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <h1>ABOUT US</h1>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="ThankWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 TextBox">
                    <?php echo get_field('thank_text'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer();
?>