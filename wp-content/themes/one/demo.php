<?php 
    /*
        Template name: Schedule Demo
    */
    get_header();
?>
<section>
    <div class="inner-banner section" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <h1><?php echo get_field('banner_text'); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="WorksWrapper main-raw">
        <div class="container">
            <div class="row">
				<div class="col-sm-6 col-xs-12 TextBoxWrap">
	<style type="text/css">
ul.demo_list { 
	list-style: initial;
	margin: initial;
    padding: 20px 0 0 40px;
    font-size: 20px;
    font-weight: 400;
}
ul.demo_list li { 
	display: list-item;
	list-style-type: disc;
    padding-bottom: 20px;
}
ul.demo_list a {
    color: rgb(117, 80, 168);
}
ul.demo_list a:hover {
    color: rgb(142, 97, 204);
}

	</style>
<h2><?php echo get_field('title'); ?></h2>

<?php the_content(); ?>

                </div>
				<div class="col-sm-6 col-xs-12 TextBoxWrap"><?php echo get_field('form_embed'); ?></div>
			</div>
        </div>
	</div>


       <div class="SubscribeWrap main-raw">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-center">
                                                <h2>Want to learn how Online Pre-Admission <br>has helped ASCs to deliver great care<span class="Question">?</span></h2>
                    <div class="text-wrap">
                    <h6>SUBSCRIBE TO OUR NEWSLETTER NOW</h6>
                    <div class="download-form"><!--[if lte IE 8]>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
<![endif]-->
<script>
  hbspt.forms.create({
	portalId: "5271895",
	formId: "8fe66494-93db-4a77-b881-e289795b113e",
	sfdcCampaignId: "7014A0000013fuAQAQ"
});
</script>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

</section>
<?php 
    get_footer();
?>