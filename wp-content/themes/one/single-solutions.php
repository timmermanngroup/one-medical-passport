<?php 
    get_header();
    /* prep video embed code if displaying Pre Admission solution, otherwise use How It Works image */
    if ($post->post_name === 'online-pre-admissions')
    {
    	$rt_col_content = '<div class="col-sm-6 col-xs-12 ImgWrap">
                    <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="https://player.vimeo.com/video/347365550" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
                </div>
';
		$lt_col_class = 'col-sm-6';
    } elseif ($post->post_name === 'patient-engagement')
    {
    	$rt_col_content = '<div class="col-sm-6 col-xs-12 ImgWrap">
                    <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="https://player.vimeo.com/video/348419780" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
                </div>
';
		$lt_col_class = 'col-sm-6';
    } elseif ($post->post_name === 'patient-tracking-boards')
    {
    	$rt_col_content = '<div class="col-sm-6 col-xs-12 ImgWrap">
                    <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="https://player.vimeo.com/video/363904367" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
                </div>
';
		$lt_col_class = 'col-sm-6';
    } elseif ($post->post_name === 'collaborative-scheduling-booking')
    {
    	$rt_col_content = '<div class="col-sm-6 col-xs-12 ImgWrap">
                    <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="https://player.vimeo.com/video/389079200" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
                </div>
';
		$lt_col_class = 'col-sm-6';
    } elseif ($post->post_name === 'online-document-management')
    {
    	$rt_col_content = '<div class="col-sm-6 col-xs-12 ImgWrap">
                    <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="https://player.vimeo.com/video/396539137" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
                </div>
';
		$lt_col_class = 'col-sm-6';
    } else
    {
		$rt_col_content = '<div class="col-sm-4 col-xs-12 ImgWrap">
                    <img src="'.get_field('how_it_works_image').'" alt="">
                </div>
';
/*
	<!-- postname: <?php echo $post->post_name; ?> -->
*/
		$lt_col_class = 'col-sm-8';
    }
    
?>
<section>
    <div class="inner-banner SolutionsBanner section" style="background: url(<?php echo get_field('banner_image'); ?>)">
        <div class="inner-main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                        <?php echo get_field('banner_text'); ?>
                        <a data-toggle="modal" data-target="#schedule-modal" class="btn-primary"><?php echo get_field('banner_cta_label'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="WorksWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center TextBoxWrap">
                    <?php echo get_field('after_banner_title'); ?>
                </div>
                <div class="<?php echo $lt_col_class; ?> col-xs-12">
                    <div class="Icon">
                        <img src="<?php echo get_field('how_it_works_icon'); ?>" alt="">
                    </div>
                    <div class="TextWrap">
                        <div class="title">
                            <h2><?php echo get_field('how_it_works_title'); ?></h2>
                        </div>
                        <?php echo get_field('how_it_works_text'); ?>
                    </div>
                </div>
                <?php echo $rt_col_content; ?>
            </div>
        </div>
    </div>

    <div class="LifeEasierWrap Col3Wrap CollaborativeWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center title">
                    <h2><?php echo get_field('why_it_makes_title'); ?></h2>
                </div>
            </div>
            <div class="row">
                <?php

                if( have_rows('why_it_makes_features') ):
                    while ( have_rows('why_it_makes_features') ) : the_row();
                    ?>
                <div class="col-sm-4 col-xs-12 BoxWrap">
                    <div class="Box">
                        <div class="ImgBox">
                            <img src="<?php echo get_sub_field('feature_image'); ?>" alt="simplified-img">
                        </div>
                        <div class="TextBox">
                            <div class="Icon">
                                <img src="<?php echo get_sub_field('feature_icon'); ?>" alt="simplified-icon">
                            </div>
                            <h4><?php echo get_sub_field('feature_title'); ?></h4>
                            <p><?php echo get_sub_field('feature_text'); ?></p>
                        </div>
                    </div>
                </div>
                <?php 
                    endwhile;
                endif;    
                ?>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center">
                    <a data-toggle="modal" data-target="#schedule-modal" class="btn-primary">SCHEDULE A DEMO</a>
                </div>
            </div>
        </div>
    </div>

    <div class="CommunityWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center title">
                    <h2><?php echo get_field('community_say_title'); ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel slide carousel-fade" id="quote-carousel">
                        <div class="carousel-inner text-center">
                            <?php

                        if( have_rows('community_testimonials') ):
                            $k =1;
                            while ( have_rows('community_testimonials') ) : the_row();
                            ?>
                            <div class="item <?php if($k == 1){ ?>active<?php } ?>" style="background-image: url(<?php echo get_sub_field('testimonial_image'); ?>);">
                                <blockquote>
                                    <?php echo get_sub_field('testimonial_text'); ?>
                                    <h5><?php echo get_sub_field('author'); ?> <br><span><?php echo get_sub_field('author_designation'); ?></span></h5>
                                    <h6><?php echo get_sub_field('author_location'); ?></h6>
                                </blockquote>
                                <?php if(get_sub_field('testimonial_video')){ 
                                       $link_array = explode('/',get_sub_field('testimonial_video'));
                                       $youid = end($link_array);    
                                ?>
                                <a href="<?php echo get_sub_field('testimonial_video'); ?>" data-youtube-id="<?php echo $youid; ?>" class="video-thumb js-trigger-video-modal VideoBtn">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/video-icon.png" alt="play-btn">
                                </a>
                                <?php } ?>
                            </div>
                            <?php 
                            $k++;
                            endwhile;
                        endif;    
                            ?>
                        </div>
                        <ol class="carousel-indicators">
                            <?php

                        if( have_rows('community_testimonials') ):
                            $l =0;
                            while ( have_rows('community_testimonials') ) : the_row();
                            ?>
                            <li data-target="#quote-carousel" data-slide-to="<?php echo $l; ?>" <?php if($l == 0){ ?>class="active" <?php } ?>></li>
                           <?php
                            $l++;
                            endwhile;
                            endif;
                           ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="AlreadyWrapper main-raw">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-right">
                        <?php dynamic_sidebar('first-cta'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="SubscribeWrap main-raw">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 text-center">
                        <?php //dynamic_sidebar('second-cta'); ?>
                        <h2><?php echo get_field('cta_tagline'); ?><span class="Question">?</span></h2>
                    <div class="text-wrap">
                    <h6>SUBSCRIBE TO OUR NEWSLETTER NOW</h6>
                    <div class="download-form"><?php ## echo do_shortcode("[email-subscribers-form id='1']"); ?>
<!--[if lte IE 8]>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
<![endif]-->
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
<script>
  hbspt.forms.create({
	portalId: "5271895",
	formId: "8fe66494-93db-4a77-b881-e289795b113e",
	sfdcCampaignId: "7014A0000013fuAQAQ"
});
</script>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
</section>
 
<?php 
    get_footer();
?>