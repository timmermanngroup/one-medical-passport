<?php get_header(); ?>
<section>
        <div class="inner-banner blog-details section" style="background: url(<?php echo get_field('banner_image'); ?>)">
            <div class="inner-main">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 inner-con fadeInUp wow">
                            <h1><?php echo get_the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="details-wrap main-raw">
            <div class="container">
                <div class="row details-main">
                    <div class="col-sm-8 col-xs-12">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="text-wrap">
                            <?php $img_url =  wp_get_attachment_url( get_post_thumbnail_id()); ?>
                                    <img src="<?php echo $img_url ?>" alt="<?php echo get_the_title(); ?>" class="img-responsive">
                            
                            <?php the_content(); ?>
                        </div>
                        <?php endwhile; endif; ?>
                    </div>
                    <div class="col-sm-4 col-xs-12 right-bar">
                        <div class="posts-sidebar">
                            <div class="title text-center">
                                <h2>Recent Posts</h2>
                            </div>
                            <ul>
                                <?php 
                                    // the query
                                    

                                    $args = array(
                                        'post_type' => 'post',
                                        'posts_per_page' => 5,
                                        'order'=> 'DESC'
                                    );
                                    $the_query = new WP_Query( $args ); ?>

                                    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <li>
                                    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                </li>
                              <?php 
                                    endwhile;
                                endif;
                                ?>  
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<div class="AlreadyWrapper main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-right">
                    <?php dynamic_sidebar('first-cta'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="SubscribeWrap main-raw">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center">
                    <?php //dynamic_sidebar('second-cta'); ?>
                    <h2><?php echo get_field('cta_tagline'); ?><span class="Question">?</span></h2>
                    <div class="text-wrap">
                    <h6>SUBSCRIBE TO OUR NEWSLETTER NOW</h6>
                    <div class="download-form"><?php echo do_shortcode("[email-subscribers-form id='1']"); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
<?php get_footer(); ?>