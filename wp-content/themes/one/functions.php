<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup() {
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'search-form' ) );
global $content_width;
if ( ! isset( $content_width ) ) { $content_width = 1920; }
register_nav_menus( array( 'main-menu' => esc_html__( 'Main Menu', 'blankslate' ) ) );
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts() {
	wp_enqueue_style( 'blankslate-style', get_stylesheet_uri() );
	wp_enqueue_style( 'blankslate-animate', get_template_directory_uri() . '/css/animate.css' ); 
	wp_enqueue_style( 'blankslate-fonts', get_template_directory_uri() . '/css/fonts.css' ); 
	wp_enqueue_style( 'blankslate-custom-style', get_template_directory_uri() . '/css/style.css' ); 
	wp_enqueue_style( 'blankslate-responsive', get_template_directory_uri() . '/css/responsive.css' ); 
	wp_enqueue_style( 'blankslate-hack', get_template_directory_uri() . '/css/hack.css' ); 

	// wp_enqueue_script( 'jquery' );

    wp_register_script('jquery.min', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array('jquery'), '', false);
    wp_enqueue_script('jquery.min');
    
    wp_register_script('bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '', true);
    wp_enqueue_script('bootstrap.min');
    
    wp_register_script('wow.min', 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js', array('jquery'), '', true);
    wp_enqueue_script('wow.min');
    

    if(get_the_ID() == 233)
	{
		wp_register_script('map', get_template_directory_uri() . '/js/map.js', array('jquery'), '', true);
		wp_enqueue_script('map');

		wp_register_script('mapsgoo', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdpTDgWRhSh9cQvMkAQqAx7i4HYfuO_U&callback=initMap', array('jquery'), '', true);
		wp_enqueue_script('mapsgoo');
    }


    wp_register_script('float-panel', get_template_directory_uri() . '/js/float-panel.js', array('jquery'), '', true);
    wp_enqueue_script('float-panel');
    
    wp_register_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true);
    wp_enqueue_script('main');
}
add_action( 'wp_footer', 'blankslate_footer_scripts' );
function blankslate_footer_scripts() {
?>
<script>
jQuery(document).ready(function ($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
$("html").addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
});
</script>
<?php
}
add_filter( 'document_title_separator', 'blankslate_document_title_separator' );
function blankslate_document_title_separator( $sep ) {
$sep = '|';
return $sep;
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '...';
} else {
return $title;
}
}
add_filter( 'the_content_more_link', 'blankslate_read_more_link' );
function blankslate_read_more_link() {
if ( ! is_admin() ) {
return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">...</a>';
}
}
add_filter( 'excerpt_more', 'blankslate_excerpt_read_more_link' );
function blankslate_excerpt_read_more_link( $more ) {
if ( ! is_admin() ) {
global $post;
return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">...</a>';
}
}
add_filter( 'intermediate_image_sizes_advanced', 'blankslate_image_insert_override' );
function blankslate_image_insert_override( $sizes ) {
unset( $sizes['medium_large'] );
return $sizes;
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init() {
register_sidebar( array(
'name' => esc_html__( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => '</li>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => esc_html__( 'First CTA', 'blankslate' ),
'id' => 'first-cta',
'before_widget' => '',
'after_widget' => '',
'before_title' => '',
'after_title' => '',
) );

register_sidebar( array(
'name' => esc_html__( 'Second CTA', 'blankslate' ),
'id' => 'second-cta',
'before_widget' => '',
'after_widget' => '',
'before_title' => '',
'after_title' => '',
) );


}
add_action( 'wp_head', 'blankslate_pingback_header' );
function blankslate_pingback_header() {
if ( is_singular() && pings_open() ) {
printf( '<link rel="pingback" href="%s" />' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
}
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script() {
if ( get_option( 'thread_comments' ) ) {
wp_enqueue_script( 'comment-reply' );
}
}
function blankslate_custom_pings( $comment ) {
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'blankslate_comment_count', 0 );
function blankslate_comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$get_comments = get_comments( 'status=approve&post_id=' . $id );
$comments_by_type = separate_comments( $get_comments );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

/* * *********************************************************
 * 
 * Acf option page
 * 
 * ********************************************************* */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'General Options',
        'menu_title' => 'General Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

/***********************************************************
 * 
 * Allow SVG upload
 * 
 ***********************************************************/

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');


add_action( 'init', 'codex_solutions_init' );
/**
 * Register a solutions post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_solutions_init() {
	$labels = array(
		'name'               => _x( 'Solutions', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Solutions', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Solutions', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Solution', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'solutions', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Solution', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Solution', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Solution', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Solution', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Solution', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Solution', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Solution:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Solutions found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Solutions found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'solutions' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'solutions', $args );
}

add_shortcode('service_title','get_service_title');
function get_service_title(){
	if(is_singular( 'solutions' )){
		return get_the_title();
	}
	else{
		return "product name";
	}
}